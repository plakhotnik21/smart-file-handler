package plakhotnik.parser;

import java.io.File;

import plakhotnik.Result;

public interface Parser {

    Result parse(File file, String string);

    boolean isSupportFile(String string);
}
