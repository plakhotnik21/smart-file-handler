package plakhotnik.parser;

import java.io.File;
import java.util.List;
import plakhotnik.Result;
import plakhotnik.reader.FileReader;

public class TxtParser implements Parser {

    private FileReader fileReader = new FileReader();

    @Override
    public Result parse(File file, String textForSearch) {

        Result result = new Result();

        int count = 0;

        try {

            List<String> lines = fileReader.readLines(file);

            for (String line : lines) {

                count++;

                if (line.contains(textForSearch)) {

                    result.addMatch(line);
                    result.addRightLineCounter(count);

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    @Override
    public boolean isSupportFile(String fileName) {

        return fileName.endsWith(".txt");

    }
}
