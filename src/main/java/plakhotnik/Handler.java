package plakhotnik;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import plakhotnik.parser.Parser;
import plakhotnik.parser.TxtParser;
import plakhotnik.parser.XmlParser;

public class Handler {

    private List<Parser> parsers;

    public Handler() {

        this.parsers = new ArrayList<>();
        this.parsers.add(new TxtParser());
        this.parsers.add(new XmlParser());

    }

    public Result handler(File file, String textForSearching) {

        Result result = new Result();

        for (Parser parser : parsers) {

            if (parser.isSupportFile(file.getName())) {

                result.merge(parser.parse(file, textForSearching));
            }

        }

        return result;
    }
}
