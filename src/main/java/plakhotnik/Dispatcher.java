package plakhotnik;

import java.io.File;
import java.io.FileNotFoundException;

import plakhotnik.exceptions.NotEnoughArgumentsException;
import plakhotnik.exceptions.NotSearchingTextException;
import plakhotnik.view.ConsoleView;
import plakhotnik.view.View;



public class Dispatcher {

    private View console;

    private Handler handler;

    private ViewResolver viewResolver;

    private File file;

    private String textForSearching;

    public Dispatcher(String[] args) throws Exception {


        if (args.length < 2) {

            throw new NotEnoughArgumentsException("Недостаточно аргументов");

        }

        this.file = new File(args[args.length - 1]);
        this.textForSearching = args[args.length - 2];

        if (textForSearching == null || textForSearching.isEmpty()) {

            throw new NotSearchingTextException("Отсутсвует текст для поиска");

        }

        if (args[args.length - 1] == null || !args[args.length - 1].contains(":")
            || !file.exists()) {

            throw new FileNotFoundException("Отсутсвует путь к файлу");

        }

        this.console = new ConsoleView();
        this.handler = new Handler();
        this.viewResolver = new ViewResolver();

    }

    public void dispatch() {

        Result result;

        if (file.isFile()) {

            result = handler.handler(file, textForSearching);
            System.out.println(viewResolver.resolve(result, textForSearching, file));
        }

        if (file.isDirectory()) {

            File[] fileArray = file.listFiles();

            if (fileArray != null) {

                for (File directoryFile : fileArray) {

                    result = handler.handler(directoryFile, textForSearching);

                    if (result.getTotalCount() > 0) {
                        System.out.println(viewResolver.resolve(result, textForSearching,
                                directoryFile));
                    }

                }

            }

        }

    }
}
