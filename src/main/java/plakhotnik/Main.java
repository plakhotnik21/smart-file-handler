package plakhotnik;

public class Main {

    public static void main(String[] args) {

        try {
            Dispatcher dispatcher = new Dispatcher(args);

            dispatcher.dispatch();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
