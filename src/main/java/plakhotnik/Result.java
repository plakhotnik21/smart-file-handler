package plakhotnik;

import java.util.ArrayList;
import java.util.List;

public class Result {

    private Integer totalCount = 0;

    private List<String> matches = new ArrayList<>();

    private List<Integer> rightLineCounter = new ArrayList<>();

    public Integer getTotalCount() {

        return totalCount;

    }

    public List<String> getMatches() {

        return matches;

    }

    public List<Integer> getRightLineCounter() {

        return rightLineCounter;

    }

    public void addRightLineCounter(int count) {

        rightLineCounter.add(count);

    }

    public void addMatch(String match) {

        matches.add(match);
        totalCount++;

    }

    public void merge(Result result) {

        matches.addAll(result.getMatches());
        rightLineCounter.addAll(result.getRightLineCounter());
        totalCount = matches.size();

    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < matches.size(); i++) {
            stringBuilder.append("[").append(rightLineCounter.get(i)).append("] ")
                    .append(matches.get(i)).append("\n");

        }
        return stringBuilder.toString();
    }
}
