package plakhotnik;

import java.io.File;

public class ViewResolver {

    private String template;

    public String resolve(Result result, String textForSearching, File file) {

        template = "Text <".concat(textForSearching).concat(">").concat(" was found ")
                .concat(Integer.toString(result.getTotalCount()))
                .concat(" time(s) in next file(s): \n").concat(file.getAbsolutePath())
                .concat("\n").concat(result.toString());
        // искомый текст %s был найден %d раз в файл:
        //%s:
        // [%d] строка бла бла бла %textForSearching
        //%s:
        // [%d] строка бла бла бла %textForSearching
        return template;
    }
}
