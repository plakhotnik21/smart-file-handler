package plakhotnik.exceptions;

public class NotEnoughArgumentsException extends Exception {

    public NotEnoughArgumentsException(String message) {

        super(message);

    }

}
