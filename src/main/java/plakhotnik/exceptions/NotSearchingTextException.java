package plakhotnik.exceptions;

public class NotSearchingTextException extends Exception {

    public NotSearchingTextException(String message) {

        super(message);

    }

}
